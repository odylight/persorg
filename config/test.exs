import Config

# Configure your database
config :persorg, Persorg.Repo,
  username: "postgres",
  password: "postgres",
  database: "persorg_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :persorg, PersorgWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
