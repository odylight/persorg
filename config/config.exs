# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :persorg,
  ecto_repos: [Persorg.Repo]

# Configures the endpoint
config :persorg, PersorgWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "9a1z9JLRCHLCmDTeefMbwkhHGzUV6vLaHvo2Xofyr2GGOMFtierS+PL7C07phDkl",
  render_errors: [view: PersorgWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: Persorg.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "9WsYwzPO"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
