defmodule Persorg.Repo do
  use Ecto.Repo,
    otp_app: :persorg,
    adapter: Ecto.Adapters.Postgres
end
