import S from "skylake"

export default class Parallax {
    constructor(opts) {
        this.els = opts.els; 
        this.elsL = this.els.length; 
        this.initialEase = opts.ease; 
        this.ease = this.initialEase; 
        this.pageHeight = S.Selector.el(opts.pageHeight); 
        this.scrollY = opts.scrollY;
        this.scrollTop = opts.scrollTop; 
        this.elsArr = [];
        
        for (let i = 0; i < this.elsL; i++) this.elsArr[i] = {
            element: S.Selector.el(this.els[i].selector),
            speed: this.els[i].speed
        };

        this.body = S.Dom.body; 
        this.targ = 0; 
        this.curr = 0; 
        this.currRounded = 0; 
        this.offIsCalled = false;
        this.isMoving = false; 
        this.rafLoop = new S.RafIndex; 
        this.rafCheckEnd = new S.RafIndex;
        S.BindMaker(this, ["eventScrollCallback", "loop", "checkEnd", "addListeners", "getScrollTop", "updateBodyHeight"]); 
        this.updateBodyHeight(); 
        this.eventRObodyh = new S.RO({
            callback: this.updateBodyHeight,
            throttle: {
                delay: 100,
                endOnly: true
            }
        }); 
        this.eventRObodyh.on(); 
        this.eventScroll = new S.Scroll({
            callback: this.eventScrollCallback,
            throttle: {
                delay: 0,
                endOnly: false
            }
        }); 
        this.scrollTop && this.addListeners()
        
    }
    on() {
        this.eventScroll.on()
    }
    off(e) {
        this.scrollTopCallback = e 
        this.offIsCalled = true 
        this.scrollTop && this.removeListeners()
        this.isMoving || this.beforeCallback()
    }
    eventScrollCallback(e, t) {
        this.delta = t 
        this.targ = -e 
        this.scrollY && this.scrollY(e) 
        this.isMoving || this.rafLoop.start(this.loop)
    }
    loop() {
        this.isMoving = true 
        this.curr += (this.targ - this.curr) * this.ease 
        this.currRounded = +this.curr.toFixed(1) 
        this.update(this.currRounded) 
        this.currRounded === this.targ ? (this.update(this.targ), this.isMoving = false) : this.rafLoop.start(this.loop) 
        this.offIsCalled && this.beforeCallback()
    }
    beforeCallback(callback) {
        function e() {
            S.WTDisable.on(), 
            this.targ = 0, 
            this.rafCheckEnd.start(this.checkEnd)
        }
        if (this.offIsCalled = false, this.ease = .35, this.scrollTopCallback) {
            var i = {
                totalHeight: this.body.offsetHeight,
                callback: callback
            };
            S.ScrollToTop(i)
        } else S.WTDisable.on()
        this.getCallback()
    }

    checkEnd() {
        this.isMoving ? this.rafCheckEnd.start(this.checkEnd) : this.getCallback()
    }

    getCallback(){
        this.rafLoop.cancel(), 
        this.rafCheckEnd.cancel(), 
        this.eventRObodyh.off(), 
        this.eventScroll.off(), 
        this.scrollTopCallback ? (this.ease = this.initialEase, S.WTDisable.off(), this.scrollTopCallback()) : this.body.style.height = ""
    }
    update(ease){
        for (let t = 0; t < this.elsL; t++) ! function (opts) {
            const i = opts.element[0].style;
            const o = "matrix3d(1,0,0,0,0,1,0,0,0,0,1,0,0," + ease * opts.speed + ",0,1)";
            i.webkitTransform = o,
            i.transform = o
        }(this.elsArr[t])
    }
    addListeners(){
        this.listeners("add")
    }
    removeListeners(){
        this.listeners("remove")
    }
    listeners(e){
        S.Listen(this.scrollTop, e, "click", this.getScrollTop)
    }
    getScrollTop(){
        this.removeListeners();
        this.off(function (t) {
            this.on(),
            this.addListeners()
        })
    }
    updateBodyHeight(){
        const e = this.pageHeight[0].offsetHeight;
        this.body.style.height = e + "px"
    }
}
