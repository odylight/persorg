import S from "skylake"

export default class HomeMenu {
    constructor() {
        S.BindMaker(this, ["menuOpen", "menuClose"])
    }
    init(t) {
        this.first = !1
        this.listeners("add")
    }
    listeners(t) {
        S.Listen("#nav-link-submenu", t, "mouseenter", this.menuOpen)
        S.Listen("#nav-link-submenu", t, "mouseleave", this.menuClose)
    }
    menuOpen(t) {
        this.first = !0
        this.isOver = !0
        S.Geb.id("nav-container").className = "active"
        this.isOver && !this.isAnimated && this.open()
    }
    menuClose(t) {
        this.first && (this.isOver = !1, S.Geb.id("nav-container").className = "", this.isOver || this.isAnimated || this.close())
    }
    open(t) {
        let i = this

        function s() {
            i.morph1Animation = new S.Morph({
                type: "path",
                element: S.Geb.id("nav-morph-path"),
                end: "M 0,0 L 10,0 L 10,10 C 10,10 10,10 5,10 C 0,10 0,10 0,10 Z",
                duration: 600,
                ease: "ExpoOut",
                callback: t => {
                    i.isAnimated = !1
                    i.isOver || i.close()
                }
            })
            i.morph1Animation.play()
        }
        this.isAnimated = !0
        S.Geb.id("nav-wrap").className = "active"
        S.Geb.id("nav-morph-path").setAttribute("d", "M 0,0 L 10,0 L 10,0 C 10,0 10,0 5,0 C 0,0 0,0 0,0 Z")
        this.morphAnimation = new S.Morph({
            type: "path",
            element: S.Geb.id("nav-morph-path"),
            end: "M 0,0 L 10,0 L 10,0 C 10,0 10,5 5,5 C 0,5 0,0 0,0 Z",
            duration: 300,
            ease: "Power3In",
            callback: s
        })
        const tl = new S.Timeline()
        tl.from("#nav-submenu-extend-bottom", "3dy", -200, 0)
        tl.from("#nav-submenu-extend-left", "3dy", -200, 0)
        tl.from(".nav-submenu-link-title", "3dy", -100, 0, 500, "Power4Out", {
            delay: 400
        })
        tl.from(".nav-submenu-link-no", "opacity", -100, 0, 500, "Power4Out", {
            delay: 50
        })
        tl.play()
        this.morphAnimation.play()
    }

    close(t) {
        let i = this

        function s() {
            i.morph3Animation = new S.Morph({
                type: "path",
                element: S.Geb.id("nav-morph-path"),
                end: "M 10,0 L 10,0 C 10,0 10,0 5,0 C 0,0 0,0 0,0 L 0,0 Z",
                duration: 600,
                ease: "ExpoOut",
                callback: t => {
                    i.isAnimated = !1
                    i.isOver && i.open()
                }
            })
            i.morph3Animation.play()
        }
        this.isAnimated = !0
        S.Geb.id("nav-wrap").className = ""
        S.Geb.id("nav-morph-path").setAttribute("d", "M 10,0 L 10,10 C 10,10 10,10 5,10 C 0,10 0,10 0,10 L 0,0 Z")
        this.morph2Animation = new S.Morph({
            type: "path",
            element: S.Geb.id("nav-morph-path"),
            end: "M 10,0 L 10,10 C 10,10 10,5 5,5 C 0,5 0,10 0,10 L 0,0 Z",
            duration: 300,
            ease: "Power3In",
            callback: s
        })
        const tl = new S.Timeline()
        tl.from("#nav-submenu-extend-left", "3dy", 0, -200)
        tl.from(".nav-submenu-link-title", "3dy", 0, -100, 160, "Power2In")
        tl.from(".nav-submenu-link-no", "3dy", 0, -100, 160, "Power2In")
        tl.from("#nav-submenu-extend-bottom", "3dy", 0, -200, {
            delay: 160
        })
        tl.play()
        this.morph2Animation.play()
    }

    destroy(t) {
        this.listeners("remove")
        this.morphAnimation && this.morphAnimation.pause()
        this.morph1Animation && this.morph1Animation.pause()
        this.morph2Animation && this.morph2Animation.pause()
        this.morph3Animation && this.morph3Animation.pause()
    }
}

const homesvg = new HomeMenu()
homesvg.init()