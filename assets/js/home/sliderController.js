import S from "skylake"
import HomeMenu from "./menu"
import Parallax from "./Parallax"
import { slider } from "./sliderMorph"



export default class SliderController {
    constructor() {
        S.BindMaker(this, ["addListeners", "getHeaderSlider"])
    }
    init(e) {
        // this.errorScrollY = new ErrorScrollY 
        this.homeSubmenu = new HomeMenu 
        this.parallax = new Parallax({
            els: [
                {
                    selector: "#app",
                    speed: 1
                }, {
                    selector: "#a-header-title",
                    speed: .15
                }, {
                    selector: "#a-header-letter-img-0",
                    speed: .05
                }, {
                    selector: "#a-header-letter-img-1",
                    speed: .05
                }, {
                    selector: "#a-header-letter-img-2",
                    speed: .05
                }, {
                    selector: "#a-header-txt",
                    speed: .02
                }
            ],
            ease: .19,
            pageHeight: ".page-height",
            // scrollY: function (e) {
            //     this.errorScrollY.init(e)
            // },
            // scrollTop: "#footer-line"
        }) 
        this.parallax.on() 
        this.homeSubmenu.init() 
        this.addListeners()
    }
    addListeners() {
        this.listeners("add")
    }
    listeners(e) {
        S.Listen(".a-header-arrow-btn", e, "click", this.getHeaderSlider)
        // S.Listen(".a-qr-nav-line-wrap", e, "click", this.getQrSlider)
    }
    getHeaderSlider(e) {
        this.listeners("remove");
        slider.init(e, this.addListeners)
    }
    // getQrSlider(e) {
    //     this.listeners("remove")
    //     QrSlider.init(e, this.addListeners)
    // }

    destroy(e, t) {
        this.listeners("remove")
        this.parallax.off()
        // this.errorScrollY.destroy()
        this.homeSubmenu.destroy()
        // Transition.init(e)
    }
}

let ctrl = new SliderController()
ctrl.init()