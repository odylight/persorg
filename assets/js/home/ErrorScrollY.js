import S from "skylake"
export default class ErrorScrollY {

    constructor() {
        this.footerLine = S.Geb.id("footer-line")
        S.BindMaker(this, [
            "calculate"
        ])
        this.RO = new S.RO({
            throttle: {
                delay: 100,
                atEnd: false
            },
            callback: this.calculate
        })
        this.calculate()
        this.RO.on()
    }
    
    init(e) {
        if (e > this.footerLineTop && "" === this.footerLine.className) {
            this.footerLine.className = "active";
            var t = new S.Timeline;
            t.from("#footer-line-left", "3dx", -100, 0, 1600, "Power5Out")
            t.from("#footer-line-right", "3dx", 100, 0, 1600, "Power5Out")
            t.play()
        }
    } 
    
    calculate() {
        var e = S.Win.h;
        this.footerLineTop = this.footerLine.getBoundingClientRect().top - 1.2 * e
    }

    destroy() {
        this.RO.off()
    }
}

let errorScrollY = new ErrorScrollY()
errorScrollY.init()