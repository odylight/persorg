import ErrorScrollY from "./ErrorScrollY"
import HomeMenu from "./menu"

export default class ErrorController {

    constructor() {}
    
    init(e) {
        let t = this;
        this.errorScrollY = new ErrorScrollY,
        this.homeSubmenu = new HomeMenu,
        this.parallax = new Parallax({
            els: [{
                selector: "#app",
                speed: 1
            }, {
                selector: "#p404-center-pillar",
                speed: -.06
            }, {
                selector: "#p404-center-damage",
                speed: .01
            }, {
                selector: "#p404-center-no",
                speed: -.2
            }],
            ease: .19,
            pageHeight: ".page-height",
            scrollY: function (e) {
                t.errorScrollY.init(e)
            },
            scrollTop: "#footer-line"
        }),
        this.parallax.on(),
        this.homeSubmenu.init(),
        this.addListeners()
    } 
    
    calculate() {
        let e = S.Win.h;
        this.footerLineTop = this.footerLine.getBoundingClientRect().top - 1.2 * e
    }

    destroy() {
        this.RO.off()
    }
}

let error = new ErrorController()
error.init()