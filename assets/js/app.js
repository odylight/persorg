// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import css from "../css/app.scss"

// webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".
//
// Import dependencies
//
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative paths, for example:
// import socket from "./socket"

// import S from "skylake"
// import "./home/Parallax"
// import "./home/ErrorScrollY"
// import {homesvg} from "./home/menu"
// import {error} from "./home/ErrorController"
// import {slider} from "./home/sliderMorph"
// import {ctrl} from "./home/sliderController"

import {
    TweenMax,
    TimelineMax
} from "gsap"
import Parallax from "./home/Parallax"
// import ErrorScrollY from "./home/ErrorScrollY"
import HomeMenu from "./home/menu"
// import ErrorController from "./home/ErrorController"
import HeaderSlider from "./home/sliderMorph"
import SliderController from "./home/sliderController"
